//
//  Photo.h
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Photo : NSObject {
    
    NSString* photoTitle;
    NSString* photoUrl;
    NSString* thumbnailUrl;
    UIImage *photo;
    
}

- (id)initWithPhotoTitle:(NSString*)thisTitle
               photoUrl:(NSString*)thisUrl
           thumbnailUrl:(NSString*)thisThumbnailUrl;

- (NSString*)getTitle;

- (NSString*)getURL;

- (NSString*)getThumbnailUrl;

- (UIImage*)getPhoto;

- (void)setWithPhoto:(UIImage*)thisPhoto;

@end
