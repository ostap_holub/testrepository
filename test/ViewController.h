//
//  ViewController.h
//  test
//
//  Created by Ostap Holub on 10/14/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    
    NSArray *albumList;
    NSInteger *selectedIndexPath;
    
    __weak IBOutlet UITableView *albumTableView;
}

@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, assign) int selectedID;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

- (NSArray*)startDownloadingDataFromUrl:(NSURL*)URL;
    
@end

