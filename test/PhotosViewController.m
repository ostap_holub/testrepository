//
//  PhotosViewController.m
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import "PhotosViewController.h"
#import "Photo.h"
#import "AFHTTPRequestOperation.h"
#import "SinglePhotoViewController.h"
#import "PhotoTableViewCell.h"

#define TABLE_CAPTION @"Photos"
#define TITLE_TAG @"title"
#define URL_TAG @"url"
#define THUMBNAILURL_TAG @"thumbnailUrl"
#define SEGUE_TAG @"SegueToSinglePhoto"

@interface PhotosViewController ()

@end

@implementation PhotosViewController

@synthesize index;

//---------------------------------------
#pragma mark Downloading data
//---------------------------------------

- (NSArray*)startDownloadingImagesFromUrl:(NSURL*)URL atIndex:(int)localIndex {
    
    NSURLRequest *downloadImageRequest = [NSURLRequest requestWithURL:URL];
    NSMutableArray *recievedImages = [[NSMutableArray alloc]init];
    
    AFHTTPRequestOperation *downloadImageOperation = [[AFHTTPRequestOperation alloc] initWithRequest:downloadImageRequest];
    downloadImageOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [downloadImageOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *downloadImageOperation, UIImage* responseImage) {
        
        [recievedImages addObject:responseImage];
        Photo* new_photo = [photoArray objectAtIndex:localIndex];
        [new_photo setWithPhoto:responseImage];
        
        PhotoTableViewCell *cell;
        NSIndexPath *path = [NSIndexPath indexPathForRow:localIndex inSection:0];
        cell = [photosTableView cellForRowAtIndexPath:path];
        cell.photoImageView.image = responseImage;
        
        imageArray = [recievedImages copy];
        
    } failure:^(AFHTTPRequestOperation *downloadImageOperation, NSError *error) {
        UIAlertView *downloadImageError = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Image"
                                                                     message:[error localizedDescription]
                                                                    delegate:nil
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles:nil];
        [downloadImageError show];
    }];
    [downloadImageOperation start];

    return imageArray;
}

- (NSArray*)startDownloadingDataFromUrl:(NSURL*)URL {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSMutableArray *receviedData = [[NSMutableArray alloc]init];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        for (NSDictionary *dict in responseObject) {
            [receviedData addObject:[[Photo alloc]initWithPhotoTitle:[dict valueForKey:TITLE_TAG]
                                                            photoUrl:[dict valueForKey:URL_TAG]
                                                        thumbnailUrl:[dict valueForKey:THUMBNAILURL_TAG]]];
        }
        photoArray = [receviedData copy];
        unsigned long count = [photoArray count];
        
        for (int i = 0; i < count; i++) {
            Photo *temp = [photoArray objectAtIndex:i];
            NSURL *thumbnailUrl = [NSURL URLWithString:[temp getThumbnailUrl]];
            [self startDownloadingImagesFromUrl:thumbnailUrl atIndex:i];
        }
        [photosTableView reloadData];
        
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving JSON"
                                                             message:[error localizedDescription]
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
         [alertView show];
     }];
    [operation start];
    
    return photoArray;
}

//---------------------------------------
#pragma mark TableViewDelegates
//---------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = TABLE_CAPTION;
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    photoArray = [[NSArray alloc]init];
    
    //downloading json
    NSString *firstPart = @"http://jsonplaceholder.typicode.com/photos?albumId=";
    NSString *Path = [NSString stringWithFormat:@"%@%d",firstPart, self.albumID];
    NSURL *url = [NSURL URLWithString:Path];
    
    [self startDownloadingDataFromUrl:url];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:SEGUE_TAG]) {
        SinglePhotoViewController *controller = (SinglePhotoViewController *)segue.destinationViewController;
        controller.URL = transferURL;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//---------------------------------------
#pragma mark TableViewDataSourceDelegates
//---------------------------------------

- (NSInteger)tableView :(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return [photoArray count];
}

- (UITableViewCell *)tableView :(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    PhotoTableViewCell *photoListCell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (photoListCell == nil) {
        photoListCell = (PhotoTableViewCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    Photo *p = [photoArray objectAtIndex:indexPath.row];
    photoListCell.photoTextLabel.numberOfLines = 0;
    photoListCell.photoTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    photoListCell.photoTextLabel.text = p.getTitle;
    
    if ([p getPhoto] == 0) {
        photoListCell.photoImageView.image = [UIImage imageNamed:@"Default.gif"];
    } else {
        photoListCell.photoImageView.image = p.getPhoto;
    }
   
    return photoListCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Photo *p = [photoArray objectAtIndex:indexPath.row];
    transferURL = [p getURL];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:SEGUE_TAG sender:self];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
