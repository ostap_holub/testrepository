//
//  AppDelegate.h
//  test
//
//  Created by Ostap Holub on 10/14/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

