//
//  PhotosViewController.h
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {

    NSArray *photoArray;
    NSArray *imageArray;
    NSString *transferURL;
    
    __weak IBOutlet UITableView *photosTableView;
}


@property NSInteger *index;
@property (nonatomic, assign) int albumID;

- (NSArray*)startDownloadingDataFromUrl:(NSURL*)URL;

- (NSArray*)startDownloadingImagesFromUrl:(NSURL*)URL atIndex:(int)localIndex;


@end
