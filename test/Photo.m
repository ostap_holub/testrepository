//
//  Photo.m
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import "Photo.h"

@implementation Photo

- (id)initWithPhotoTitle:(NSString *)thisTitle
                photoUrl:(NSString *)thisUrl
            thumbnailUrl:(NSString *)thisThumbnailUrl {
    if (self == [super init]) {
        photoTitle = thisTitle;
        photoUrl = thisUrl;
        thumbnailUrl = thisThumbnailUrl;
    }
    return self;
}

- (NSString *)getTitle {
    return photoTitle;
}

- (NSString* )getURL {
    return photoUrl;
}

- (NSString*)getThumbnailUrl {
    return thumbnailUrl;
}

- (UIImage *)getPhoto {
    
    if (photo) {
        return photo;
    } else return 0;
}

- (void)setWithPhoto:(UIImage*)thisPhoto {
    photo = thisPhoto;
}

@end
