//
//  SinglePhotoViewController.m
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import "SinglePhotoViewController.h"
#import "AFHTTPRequestOperation.h"

#define TABLE_CAPTION @"Photo"

@interface SinglePhotoViewController ()

@end

@implementation SinglePhotoViewController


@synthesize URL;

//---------------------------------------
#pragma mark Downloading data
//---------------------------------------

- (UIImage*)startDownloadingImageFromUrl:(NSURL*)imageURL {
    
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:imageURL];
    
    AFHTTPRequestOperation *imageOperation = [[AFHTTPRequestOperation alloc]initWithRequest:imageRequest];
    imageOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [imageOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *imageOperation, id responseImage) {
        
        bigPhoto = (UIImage *)responseImage;
        [imageView setImage:bigPhoto];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
    } failure:^(AFHTTPRequestOperation *imageOperation, NSError *error) {
        UIAlertView *downloadImageError = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Image"
                                                                     message:[error localizedDescription]
                                                                    delegate:nil
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles:nil];
        [downloadImageError show];
    }];
    [imageOperation start];
    
    return bigPhoto;
}

//----------------------------------------
#pragma mark ImageViewDataSource delegates
//----------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    bigPhoto = [[UIImage alloc]init];
    self.navigationItem.title = TABLE_CAPTION;
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    NSString *localUrl = URL;
    NSURL *downloadImageURL = [NSURL URLWithString:localUrl];
    
    [self startDownloadingImageFromUrl:downloadImageURL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
