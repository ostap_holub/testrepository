//
//  ViewController.m
//  test
//
//  Created by Ostap Holub on 10/14/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import "ViewController.h"
#import "Album.h"
#import "AFHTTPRequestOperation.h"
#import "PhotosViewController.h"
#import "AlbumTableViewCell.h"

#define TITLE_TAG @"title"
#define ID_TAG @"id"
#define TABLE_CAPTION @"Albums"
#define SEGUE_TAG @"SegueToNextPage"

@interface ViewController ()

@end

@implementation ViewController

//---------------------------------------
#pragma mark Downloading data
//---------------------------------------

- (NSArray*)startDownloadingDataFromUrl:(NSURL*)URL {
    
    NSMutableArray *albums = [[NSMutableArray alloc]init];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        for (NSDictionary *dict in responseObject) {
            [albums addObject:[[Album alloc]initWithAlbumName:[dict valueForKey:TITLE_TAG]
                                                      albumId:(int)[dict valueForKey:ID_TAG]]];
        }
        albumList = [albums copy];
        [albumTableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving JSON"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
    [operation start];
    return albumList;
}

//---------------------------------------
#pragma mark TableViewDelegates
//---------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = TABLE_CAPTION;
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.isEditing = NO;
   
    static NSString *Path = @"http://jsonplaceholder.typicode.com/albums";
    NSURL *url = [NSURL URLWithString:Path];
    [self startDownloadingDataFromUrl:url];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:SEGUE_TAG]){
        PhotosViewController *controller = (PhotosViewController *)segue.destinationViewController;
        controller.albumID = self.selectedID;
    }
}

//---------------------------------------
#pragma mark TableViewDataSourceDelegates
//---------------------------------------

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray *newAlbumArray = [[NSMutableArray alloc]initWithArray:albumList];
        [newAlbumArray removeObjectAtIndex:indexPath.row];
        
        albumList = newAlbumArray;
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        
        [tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [albumList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    AlbumTableViewCell *albumTitleCell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (albumTitleCell == nil){
        albumTitleCell = (AlbumTableViewCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:simpleTableIdentifier];
    }
    
    Album *p = [albumList objectAtIndex:indexPath.row];
    albumTitleCell.albumTitleLabel.numberOfLines = 0;
    albumTitleCell.albumTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    albumTitleCell.albumTitleLabel.text = p.getTitle;
 
    return  albumTitleCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndexPath = (NSInteger*)indexPath.row;
    
    Album *selectedAlbum = [albumList objectAtIndex:indexPath.row];
    self.selectedID = [selectedAlbum getAlbumId];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:SEGUE_TAG sender:self];
}

#pragma mark editing actions

- (IBAction)plusButtonTapped:(id)sender {
    NSMutableArray *newAlbumsList = [[NSMutableArray alloc]initWithArray:albumList];
    Album *additionalAlbum = [[Album alloc]initWithAlbumName:@"Additional album" albumId:101];
    
    [newAlbumsList insertObject:additionalAlbum atIndex:0];
    albumList = newAlbumsList;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    [albumTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (IBAction)editButtonTapped:(id)sender {
    
    if (self.isEditing) {
        self.editButton.title = @"Edit";
        [albumTableView setEditing:NO animated:YES];
        self.isEditing = NO;
    } else {
        self.editButton.title = @"Done";
        [albumTableView setEditing:YES animated:YES];
        self.isEditing = YES;
    }
}

@end
