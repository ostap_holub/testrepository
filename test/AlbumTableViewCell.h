//
//  AlbumListCell.h
//  test
//
//  Created by Ostap Holub on 10/28/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *albumTitleLabel;

@end
