//
//  Album.m
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import "Album.h"


@implementation Album

- (id)initWithAlbumName:(NSString *)thisAlbumName
               albumId:(int)thisAlbumId
    {
    if (self == [super init]) {
        albumTitle = thisAlbumName;
        albumId = thisAlbumId;
    }
    return self;
}

- (NSString *)getTitle {
    return albumTitle;
}

- (int)getAlbumId {
    return albumId;
}

@end
