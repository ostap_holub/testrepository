//
//  Album.h
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Album : NSObject {
    
    NSString *albumTitle;
    int albumId;
    
}

- (id)initWithAlbumName:(NSString *)thisAlbumName
               albumId:(int)thisAlbumId;

- (NSString*)getTitle;

- (int)getAlbumId;

@end
