//
//  AlbumListCell.m
//  test
//
//  Created by Ostap Holub on 10/28/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import "AlbumTableViewCell.h"

@implementation AlbumTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
