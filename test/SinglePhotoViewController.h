//
//  SinglePhotoViewController.h
//  test
//
//  Created by Ostap Holub on 10/15/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SinglePhotoViewController : UIViewController {
    
    UIImage *bigPhoto;
    __weak IBOutlet UIImageView *imageView;
}

@property NSString* URL;

- (UIImage*)startDownloadingImageFromUrl:(NSURL*)imageURL;

@end
