//
//  PhotoListCell.h
//  test
//
//  Created by Ostap Holub on 10/28/15.
//  Copyright © 2015 Ostap Holub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *photoTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end
